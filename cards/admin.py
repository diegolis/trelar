from django.contrib import admin

from cards.models import *


@admin.register(Board)
class BoardAdmin(admin.ModelAdmin):
    pass


@admin.register(Column)
class ColumnAdmin(admin.ModelAdmin):
    list_display = ['board', 'name', 'stage']


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_filter = ['board', 'labels', 'closed']
    list_display = ['board', 'title']
    search_fields = ['title', 'description']


@admin.register(Movement)
class MovementAdmin(admin.ModelAdmin):
    list_display = ['to_column', 'timestamp', 'card']
    list_filter = ['to_column__stage', 'card__board']

@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
	pass