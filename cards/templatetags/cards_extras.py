from django import template

register = template.Library()

@register.simple_tag()
def time_function_with_args(card, time_function, end_time):
	if time_function == 'reaction_time':
		return_time = card.reaction_time(end_time).total_seconds()
	elif time_function == 'cycle_time':
		return_time = card.cycle_time(end_time).total_seconds()
	elif time_function == 'lead_time':
		return_time = card.lead_time(end_time).total_seconds()
	days = (return_time-return_time%86400)//86400
	return_time = return_time-days*86400
	hours = ((return_time)-(return_time%3600))//3600
	return "{} days, {} hours".format(days, hours)
