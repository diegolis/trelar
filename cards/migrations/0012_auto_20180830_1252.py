# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-08-30 15:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cards', '0011_member_last_sync'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='board',
            name='last_sync',
        ),
        migrations.RemoveField(
            model_name='member',
            name='last_sync',
        ),
        migrations.AddField(
            model_name='board',
            name='last_influx_sync',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='board',
            name='last_trello_sync',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='member',
            name='last_influx_sync',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
