# -*- coding: utf-8 -*-
import json

from django.utils import timezone
from django.core.management.base import BaseCommand
from django.conf import settings

from cards.models import get_trello_client


class Command(BaseCommand):

    def handle(self, *args, **options):
        client = get_trello_client()
        for board in client.list_boards(board_filter="open"):
            print(board.name, board.id)
