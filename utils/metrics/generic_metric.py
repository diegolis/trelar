import six
from datetime import datetime
import requests
import logging
import time

logger = logging.getLogger(__name__)

REQUESTS_EXCEPTIONS = (
    requests.exceptions.ConnectionError,  # A connection error ocurred.
    requests.exceptions.ConnectTimeout,   # The request timed out
    requests.exceptions.HTTPError,        # An HTTP error occurred
    requests.exceptions.ReadTimeout,      # The server did not send data in time
)

INFLUX_SUCCESS_CODE = 204

try:
    from django.conf import settings
    INFLUXDB_SETTINGS = settings.INFLUXDB_SETTINGS
except Exception:
    from .generic_metric_settings import INFLUXDB_SETTINGS


class GenericMetric(object):
    """Generic metric and base class for other metrics.

    Initialize the object with the correct values for:
    - self.metric (str)
    - self.dimensions (dict)
    - self.values (dict)
    - self.timestamp (datetime, utc)
    Then call the post_data method.
    To subclass override the __init__ method.
    """

    def __init__(self, metric_name, dimensions, values, timestamp):
        self.metric = metric_name
        self.dimensions = dimensions
        self.values = values
        self.timestamp = timestamp

    def post_data(self, timeout=10, retries=3, supress_requests_exceptions=True):
        data = u'{}{} {} {}'.format(
            self.metric,
            self._format_dimensions(),
            self._format_values(),
            self._get_datetime_in_nanoseconds()
        )
        url = '{}/write?db={}'.format(INFLUXDB_SETTINGS['db_url'],
                                      INFLUXDB_SETTINGS['db_name'])
        self.post_with_retries(data, url, timeout, retries, supress_requests_exceptions)

    def post_with_retries(self, data, url, timeout, retries, supress_requests_exceptions):
        try_number = 1
        success = False
        last_exception = None

        while try_number <= retries and not success:

            logger.info(u'Try {} of {} - POST {} {}'.format(try_number, retries, url, data))
            try_number += 1

            start = time.time()
            try:
                response = requests.post(
                    url,
                    data=data.encode('utf-8', errors='replace'),
                    auth=(INFLUXDB_SETTINGS['db_user'], INFLUXDB_SETTINGS['db_pass']),
                    timeout=timeout
                )
            except REQUESTS_EXCEPTIONS as e:
                # only raise in the last retry
                last_exception = e
                logger.error(str(e))
                print(e)
            else:
                elapsed = time.time() - start
                logger.debug('POST took {:.2f} seconds'.format(elapsed))
                if response.status_code != INFLUX_SUCCESS_CODE:
                    last_exception = GenericMetricException('POST to {} returned {}'.format(
                        url,
                        response.status_code
                    ))
                    logger.error(str(last_exception))
                else:
                    success = True

        if last_exception is not None and not success:
            if isinstance(last_exception, REQUESTS_EXCEPTIONS) and supress_requests_exceptions:
                pass
            else:
                raise last_exception

    def _format_dimensions(self):
        dims = ','.join((u'{}={}'.format(self._escape(key),
                                         self._escape(self.dimensions[key]))
                         for key in self.dimensions.keys()))
        return ',' + dims if dims else ''

    def _format_values(self):
        return ','.join((u'{}={}'.format(self._escape(key),
                                         self._quote(self.values[key]))
                         for key in self.values.keys()))

    def _escape(self, string):
        if string != None:
            return string.replace(' ', r'\ ')
        else:
            return r'\ None'

    def _quote(self, value):
        if isinstance(value, six.string_types) and ' ' in value:
            return u'"{}"'.format(value)
        else:
            return value

    def _get_datetime_in_nanoseconds(self):
        try:
            timestamp = self.timestamp.timestamp()
        except AttributeError:
            naive = self.timestamp.replace(tzinfo=None)
            epoch = datetime.utcfromtimestamp(0)
            timestamp = (naive - epoch).total_seconds()
        return int(timestamp * 10**9)


class GenericMetricException(Exception):
    """Exception class for GenericMetric"""
    pass
