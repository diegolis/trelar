from os import path
from subprocess import Popen, PIPE
import logging

from inotify.adapters import Inotify
from inotify.constants import IN_CLOSE_WRITE

logger = logging.getLogger(__name__)

def _generate_rsync_options(ignore_existing=False, show_info=False,
                            temp_dir=None, copy_links=False,
                            remove_source_files=False, ssh_opts=None,
                            compress=True):
    args = ['-rltD']

    if ignore_existing:
        args.append('--ignore-existing')
    if show_info:
        args.append('--info=progress2,name')
    if temp_dir is not None:
        args.append('--temp-dir={}'.format(temp_dir))
    if copy_links:
        args.append('-L')
    if remove_source_files:
        args.append('--remove-source-files')
    if ssh_opts is not None:
        if isinstance(ssh_opts, str):
            args.append('-e')
            args.append('ssh {}'.format(ssh_opts))
        elif isinstance(ssh_opts, list):
            ssh_opt_list = ['-o {}'.format(opt) for opt in ssh_opts]
            args.append('-e')
            args.append('ssh {}'.format(' '.join(ssh_opt_list))) 
    if compress:
        args.append('-z')

    logger.debug('rsync flags {}'.format(args))
    return args


def _run_rsync(origin, destination, **kwargs):
    rsync_cmd = ['rsync']
    rsync_cmd += _generate_rsync_options(**kwargs)
    rsync_cmd += [origin, destination]
    print(' '.join(rsync_cmd))
    return Popen(rsync_cmd, stdout=PIPE, stderr=PIPE)


def rsync(origin, destination, **kwargs):
    """Run rsync waiting for it completion.

    Additional kwargs are to add more parameters to the rsync command.
    ignore_existing         --ignore-existing
    show_info:              --info=progress2,name
    temp_dir:               --temp-dir=<param>
    copy_links:             -L
    remove_source_files:    --remove-source-files
    ssh_opts:               -e 'ssh <opts>'
    compress:               -z
    """
    process = _run_rsync(origin, destination, **kwargs)
    out, err = process.communicate()
    status = process.poll()
    return status, out, err


def rsync_async(origin, destination, **kwargs):
    """Run rsync asynchronously returning it process

    See rsync docstring for all available flags
    """
    return _run_rsync(origin, destination, **kwargs)


def watch_and_sync(directory, destination, mask=IN_CLOSE_WRITE, **kwargs):
    i = Inotify()
    i.add_watch(directory, mask=mask)

    for event in i.event_gen(yield_nones=False):
        _, type_names, fpath, filename = event

        status, out, err = rsync(
            path.join(fpath, filename),
            destination,
            **kwargs
        )

        if status != 0:
            logger.error('Error with rsync: {}'.format(err))
