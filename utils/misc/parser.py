from collections import namedtuple
from datetime import datetime
from operator import methodcaller
from os.path import splitext
from warnings import warn
import re


VIDEO_EXTENSIONS = [
   '.mp4', '.wmv', '.ts', '.webm', '.mpeg', '.mpg', '.avi',
]


OriginFilename = namedtuple(
    'OriginFilename', [
        'device', 'slot', 'date', 'time', 'ext', 'datetime'
    ]
)


def parse_comma_separated_integer_list(input_string, separator=','):
    return list(map(int, map(methodcaller('strip'),
                             input_string.split(separator))))


def parse_datetime(input_string, tzinfo=None):
    formats = {
        8: '%Y%m%d',
        10: '%Y-%m-%d',

        12: '%Y%m%d%H%M',
        14: '%Y%m%d%H%M%S',

        13: '%Y%m%dT%H%M',
        15: '%Y%m%dT%H%M%S',

        16: '%Y-%m-%dT%H:%M',  # alternatively '%Y-%m-%d %H:%M'
        19: '%Y-%m-%dT%H:%M:%S',  # alternatively '%Y-%m-%d %H:%M:%S'
        26: '%Y-%m-%dT%H:%M:%S.%f'  # alternatively '%Y-%m-%d %H:%M:%S.%f'
    }
    return datetime.strptime(input_string.replace(' ', 'T'),
                             formats[len(input_string)]).replace(tzinfo=tzinfo)


def parse_origin_datetime(date, time, tzinfo=None):
    return datetime(
        int(date[:4]),
        int(date[4:6]),
        int(date[6:]),
        int(time[:2]),
        int(time[2:4]),
        int(time[4:]),
        tzinfo=tzinfo
    )


def parse_origin(filename):
    warn('parse_origin is deprecated, use parse_raw_filename instead',
         DeprecationWarning)
    origin = parse_raw_filename(filename)
    return (origin.device, origin.slot,
            origin.date, origin.time, origin.datetime)


def parse_raw_filename(filename):
    origin_pattern = re.search(
            (r'(?P<device>\w+-?\w+)(-\d+)?'
             r'_(?P<slot>\d+)'
             r'_(?P<date>\d+)'
             r'_(?P<time>\d+)'
             r'(?P<ext>\.\w+)'),
            filename, re.M | re.I
    )
    if origin_pattern:
        device = origin_pattern.groupdict()['device']
        slot = origin_pattern.groupdict()['slot']
        date = origin_pattern.groupdict()['date']
        time = origin_pattern.groupdict()['time']
        ext = origin_pattern.groupdict()['ext']
        if len(time) == 2:
            time = time + "00"
        dt = parse_origin_datetime(date, time)
        origin = OriginFilename(device, slot, date, time, ext, dt)

        return origin


def is_video_file(filename):
    return splitext(filename)[-1] in VIDEO_EXTENSIONS
