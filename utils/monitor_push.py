from argparse import ArgumentParser, RawDescriptionHelpFormatter
try:
    from configparser import ConfigParser
except ImportError:  # Python 2.x
    from ConfigParser import SafeConfigParser as ConfigParser
import logging

from utils.sync import watch_and_sync

logger = logging.getLogger(__name__)


def _parse_arguments():
    config_parser = ArgumentParser(add_help=False)
    config_parser.add_argument('--config-file', metavar='FILE')

    arguments, remaining_argv = config_parser.parse_known_args()
    defaults = {}
    if arguments.config_file:
        config = ConfigParser()
        config.read([arguments.config_file])
        defaults = dict(config.items('Settings'))

    parser = ArgumentParser(parents=[config_parser], description=__doc__,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('--watch-dir', type=str, required=True)
    parser.add_argument('--destination', type=str, required=True)
    parser.add_argument('--remove-source-files', type=str, required=False, default=True, dest='remove')

    parser.set_defaults(**defaults)
    args = parser.parse_args(remaining_argv)
    return args


def main(*args, **kwargs):
    args = _parse_arguments()
    watch_and_sync(args.watch_dir, args.destination, remove_source_files=(args.remove == "True"),
                   temp_dir='/tmp')


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logger.info('Exiting...')
