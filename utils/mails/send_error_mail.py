import argparse
import json
import logging
import logging.config
import os
import sys
from datetime import datetime
from datetime import timedelta


sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))
from mails import config
from mails import crontab_error_mail_factory
from mails.config import LOGGING_CONF

logging.config.dictConfig(LOGGING_CONF)

logger = logging.getLogger(__name__)

LOG = os.path.expanduser('~/.crontab_error_mails.json')
DATETIME_FORMAT = '%Y%m%d%H%M%S'


def parse_delay(delay_str):
    options = [
        ('s', lambda x: timedelta(seconds=x)),
        ('m', lambda x: timedelta(minutes=x)),
        ('h', lambda x: timedelta(hours=x)),
        ('d', lambda x: timedelta(days=x))
    ]
    for option, f in options:
        if delay_str.endswith(option):
            return f(int(delay_str.rstrip(option)))


def get_log_file_contents():
    try:
        with open(LOG, 'r') as json_file:
            data = json.loads(json_file.read())
    except IOError:
        data = {}
    return data


def set_log_file_contents(data):
    with open(LOG, 'w') as json_file:
        json_file.write(json.dumps(data))


def get_last_sent_mail(failing_script):
    data = get_log_file_contents()
    last_time = data.get(failing_script)
    if last_time is None:
        return datetime.min
    else:
        return datetime.strptime(last_time, DATETIME_FORMAT)


def set_last_sent_mail(failing_script):
    data = get_log_file_contents()
    data[failing_script] = datetime.now().strftime(DATETIME_FORMAT)
    set_log_file_contents(data)


if __name__ == '__main__':
    logger.info('%s: script starts', __file__)
    logger.debug('Parsing command line arguments')

    parser = argparse.ArgumentParser(__file__)
    parser.add_argument('failing_script')
    parser.add_argument('-d', '--min-delay-between-repetitions',
                        dest='min_delay',
                        help='Time to wait between repetitions (1s, 1m, 1h, 1d)',
                        default='1h')

    args = parser.parse_args()

    logger.debug('args: %s', args)

    if not os.path.exists(args.failing_script):
        logger.error('No such file: "{}"'.format(args.failing_script))
        logger.info('%s: script ends', __file__)
        exit(1)

    min_delay_between_repetitions = parse_delay(args.min_delay)
    last_sent_mail_datetime = get_last_sent_mail(args.failing_script)
    logger.info('Last error mail for %s was sent on %s',
                args.failing_script,
                last_sent_mail_datetime)

    if datetime.now() >= last_sent_mail_datetime + min_delay_between_repetitions:
        logger.info('Creating e-mail message')
        mail_sender = crontab_error_mail_factory(config.SERVER_NAME,
                                                 config.CRONTAB_ERRORS,
                                                 args.failing_script)
        logger.info('Sending e-mail message')
        mail_sender.send()
        logger.info('Updating internal database of sent e-mails')
        set_last_sent_mail(args.failing_script)
    else:
        logger.info('Minimum delay (%s) between mails not elapsed', args.min_delay)
    logger.info('%s: script ends', __file__)
