from django.conf import settings


def get_rest_request_headers():
    return {'Authorization': 'Token {}'.format(settings.REST_AUTH_TOKEN)}
