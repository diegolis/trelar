from os import path
from os import remove
from subprocess import check_output
from subprocess import STDOUT
from tempfile import NamedTemporaryFile
from tempfile import TemporaryDirectory


try:
    from django.conf import settings
    FFMPEG = settings.FFMPEG
except Exception:
    FFMPEG = 'ffmpeg'


BASE_FFMPEG_CMD = [FFMPEG, '-hide_banner', '-nostats', '-y']


def create_audio_clip(sources, output, first_offset=None, last_offset=None):
    if len(sources) == 1:
        return _cut_audio_clip(sources[0], output, first_offset, last_offset)
    elif len(sources) > 1:
        with TemporaryDirectory() as cutting_directory:
            cmd_output = ''
            if first_offset:
                first_path = path.join(cutting_directory, 'first.mp3')
                cmd_output += _cut_audio_clip(sources[0], first_path,
                                              start_offset=first_offset)
                sources[0] = first_path
            if last_offset:
                last_path = path.join(cutting_directory, 'last.mp3')
                cmd_output += _cut_audio_clip(sources[-1], last_path,
                                              duration=last_offset)
                sources[-1] = last_path
            return cmd_output + _join_audio_clips(sources, output)


def _cut_audio_clip(source, output, start_offset=None, duration=None):
    cmd = BASE_FFMPEG_CMD + ['-i', source]
    if start_offset:
        cmd += ['-ss', str(start_offset.total_seconds())]
    if duration:
        cmd += ['-t', str(duration.total_seconds())]
    cmd += ['-vn', '-c', 'copy', output]
    return check_output(cmd, stderr=STDOUT).decode(errors='ignore')


def _join_audio_clips(sources, output):
    with NamedTemporaryFile(delete=False) as concat_file:
        concat_file.write('\n'.join(
            'file {}'.format(source) for source in sources
        ).encode('utf-8'))
    cmd = BASE_FFMPEG_CMD + ['-f', 'concat',
                             '-safe', 'false',
                             '-i', concat_file.name,
                             '-c', 'copy',
                             output]
    cmd_output = check_output(cmd, stderr=STDOUT).decode(errors='ignore')
    remove(concat_file.name)
    return cmd_output
