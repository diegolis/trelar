from __future__ import division
from re import findall
from re import match
from re import search
from subprocess import check_output
from subprocess import STDOUT

from measure_file import measure_file


try:
    from django.conf import settings
    FFMPEG = settings.FFMPEG
except Exception:
    FFMPEG = 'ffmpeg'


BASE_FFMPEG_CMD = [FFMPEG, '-hide_banner', '-nostats']


def measure_media_loss(file_path, ndb=-35, initial_values=None):
    """Calls ffmpeg and returns the ratio of valid audio and video."""
    if not initial_values:
        total_values = measure_file(file_path)
    else:
        total_values = initial_values

    if not total_values:
        return

    cmd = BASE_FFMPEG_CMD + ['-i', file_path,
                             '-vf', 'mpdecimate',
                             '-af', 'silencedetect=n={}dB:d=1'.format(ndb),
                             '-f', 'null', '-']
    output = check_output(cmd, stderr=STDOUT).decode(errors='ignore')

    if total_values['frames'] != 0:
        frames_ratio = _measure_frames_ratio(output, total_values['frames'])
    else:
        frames_ratio = 0.0

    audio_ratio = _measure_audio_ratio(output, total_values['duration'])

    return {'frames_ratio': frames_ratio, 'audio_ratio': audio_ratio}


def _measure_frames_ratio(output, total_frames):
    try:
        match = search(r'frame=\s*(?P<frames>\d+)', output).groupdict()
    except AttributeError:
        pass
    else:
        return 1 - int(match['frames']) / total_frames


def _measure_audio_ratio(output, total_duration):
    silence_duration = 0.0
    matches = findall('(silence_start.*|silence_duration.*)', output)
    for line in matches:
        try:
            ss = match('silence_duration:(?P<ss>.+)', line).groupdict()['ss']
        except (AttributeError, KeyError):
            pass
        else:
            silence_duration += float(ss)
    try:
        ss = match('silence_start:(?P<ss>.+)', line).groupdict()['ss']
    except (AttributeError, KeyError, UnboundLocalError):
        pass
    else:
        silence_duration += total_duration.total_seconds() - float(ss)

    return silence_duration / total_duration.total_seconds()
