from datetime import timedelta
from re import search
from subprocess import check_output
from subprocess import STDOUT


try:
    from django.conf import settings
    FFMPEG = settings.FFMPEG
except Exception:
    FFMPEG = 'ffmpeg'


BASE_FFMPEG_CMD = [FFMPEG, '-hide_banner', '-nostats']


def measure_file(file_path):
    """Calls ffmpeg and returns the duration as a timedelta object."""
    cmd = BASE_FFMPEG_CMD + ['-i', file_path,
                             '-codec:', 'copy',
                             '-f', 'null', '-']
    output = check_output(cmd, stderr=STDOUT).decode(errors='ignore')

    try:
        match = search(
            (r'(frame=\s*(?P<frames>\d+)\s)?.*'
             r'time=(?P<hh>\d\d):(?P<mm>\d\d):(?P<ss>\d\d)\.(?P<cc>\d\d)'),
            output
        ).groupdict()
    except AttributeError as e:
        pass
    else:
        frames = int(match['frames']) if match['frames'] else 0
        duration = timedelta(
            hours=int(match['hh']),
            minutes=int(match['mm']),
            seconds=int(match['ss']),
            microseconds=int(match['cc'])*10000
        )
        return {'frames': frames, 'duration': duration}
