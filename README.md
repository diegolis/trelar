# Trelar
Trello Metrics for Kanban teams

```
Python Version >= 3.5
```

# Local Install

1. Create a virtualenv and install `requirements.txt`
2. Copy `trelar/local_settings.py.template` in `trelar/local_settings.py`, and set the credentials for Trello connection.
3. Setup DB with `python manage.py migrate`
4. Create the boards in db (`/admin/cards/board/add/`), using info from `python manage.py list_boards`

# Google App Engine Install

1. Setup a project in GCP, and configure gcloud SDK.
2. Create a bucket `trelar-static` and upload static files:
    ```
    gsutil mb gs://trelar-static
    gsutil defacl set public-read gs://trelar-static
    python manage.py collectstatic --noinput
    gsutil rsync -R static/ gs://trelar-static/static
    ```

3. Create a DB in Google SQL and adjust `settings.py`.
4. For running migrations. Download and run `cloud_sql_proxy`:
    ```
    export DB=REMOTE; python manage.py migrate
    ```

5. Deploy the app:

    ```
    gcloud app deploy
    gcloud app deploy cron.yaml
    ```
